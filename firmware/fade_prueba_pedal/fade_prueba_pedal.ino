/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Fade
*/

int footswitch = 3;   // the digital pin the switch is attached to.
int mosfet = 9;        // the PWM pin the MOSFET gate is attached to.

int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

volatile bool flag = false;

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);

  // Setup interrupt on footswitch
  attachInterrupt(digitalPinToInterrupt(footswitch), flip, FALLING);
  //pinMode(footswitch, INPUT);
  
  // declare pin 9 to be an output:
  pinMode(mosfet, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {

  
  Serial.print("Flag status:" + String(flag));
  //int buttonState = digitalRead(footswitch);
  //Serial.println(buttonState);
  
  int pot = analogRead(A0);
  int pwm_signal = pot/4;



  // Consider button state
  if(flag){
    // Set PWM pin directly from analog read signal
    // https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/
    analogWrite(mosfet, 255-pwm_signal);
    
    // Report signal
    Serial.println(" Pot signal:" + String(255-pwm_signal));
    
  } else {
    analogWrite(mosfet, 255);

    // Report signal
    Serial.println(" Pot signal:" + String(255));
  }


  



  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}

void flip() {
  // https://forum.arduino.cc/t/debouncing-an-interrupt-trigger/45110/2
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > 200) 
  {
    Serial.println("Button pressed!");
    flag = !flag;
  }
  last_interrupt_time = interrupt_time;
}
