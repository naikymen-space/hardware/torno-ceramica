/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Fade
*/

#define LED D0            // Led in NodeMCU at pin GPIO16 (D0).

//int footswitch = D6;    // the digital (interrupt) pin the switch is attached to.
//int mosfet = D3;        // the PWM pin the MOSFET gate is attached to.
//int footpot = A0;

volatile bool flag = false;
//int interrupt_debounce_ms = 200;


void IRAM_ATTR flip_ISR() {
  // https://forum.arduino.cc/t/debouncing-an-interrupt-trigger/45110/2
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > 200) 
  {
    Serial.println("Button pressed!");
    flag = !flag;
  }
  last_interrupt_time = interrupt_time;
}

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);

  pinMode(LED, OUTPUT);    // LED pin as output.

  // Setup interrupt on footswitch
  attachInterrupt(digitalPinToInterrupt(D6), flip_ISR, FALLING);
  //pinMode(D6, INPUT);
  
  // declare pin D3 to be an output:
  pinMode(D3, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {

  
  Serial.print("Flag status:" + String(flag));
  //int buttonState = digitalRead(footswitch);
  //Serial.println(buttonState);
  
  int pot = analogRead(A0);
  int pwm_signal = 255 - (pot/4) + 1;



  // Consider button state
  if(flag){
    // Set mosfet D3 PWM pin directly from analog read signal
    // https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/
    analogWrite(D3, pwm_signal);
    
    // Report signal
    digitalWrite(LED, HIGH);
    Serial.print(" Pot signal:" + String(pot));
    Serial.println(" PWM signal:" + String(pwm_signal));
    
  } else {
    analogWrite(D3, 255);

    // Report signal
    digitalWrite(LED, LOW);
    Serial.print(" Pot signal:" + String(255));
    Serial.println(" PWM signal:" + String(pwm_signal));
  }

  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
