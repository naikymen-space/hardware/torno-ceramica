/*
  Analog input, analog output, serial output

  Reads an analog input pin, maps the result to a range from 0 to 255 and uses
  the result to set the pulse width modulation (PWM) of an output pin.
  Also prints the results to the Serial Monitor.

  The circuit:
  - potentiometer connected to analog pin 0.
    Center pin of the potentiometer goes to the analog pin.
    side pins of the potentiometer go to +5V and ground
  - LED connected from digital pin 9 to ground through 220 ohm resistor

  created 29 Dec. 2008
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogInOutSerial
*/

volatile bool flag = false;
void IRAM_ATTR flip_ISR() {
  // https://forum.arduino.cc/t/debouncing-an-interrupt-trigger/45110/2
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > 200) 
  {
    // Serial.println("Button pressed!");
    flag = !flag;
  }
  last_interrupt_time = interrupt_time;
}

// These constants won't change. They're used to give names to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

// const int ledOutPin = 9;  // Analog output pin that the LED is attached to
const int ledOutPin = LED_BUILTIN;  // Analog output pin that the LED is attached to

const int motorOutPin = D5;

const int pushButton = D7;

int sensorValue = 0;  // value read from the pot
int outputValue = 0;  // value output to the PWM (analog out)

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(115200);

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(ledOutPin, OUTPUT);

  pinMode(motorOutPin, OUTPUT);

  // Setup interrupt on footswitch
  // https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  // https://docs.arduino.cc/tutorials/generic/digital-input-pullup
  pinMode(pushButton, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pushButton), flip_ISR, FALLING);
}

void loop() {
  // Consider button state
  if(flag){
    // read the analog in value:
    sensorValue = analogRead(analogInPin);
    // map it to the range of the analog out:
    outputValue = map(sensorValue, 0, 1023, 0, 255);
    // change the analog out value:
    analogWrite(ledOutPin, outputValue);
    analogWrite(motorOutPin, 255-outputValue);
    
  } else {
    // Turn everything off.
    analogWrite(ledOutPin, 255);
    analogWrite(motorOutPin, 0);
  }

  // // read the analog in value:
  // sensorValue = analogRead(analogInPin);
  // // map it to the range of the analog out:
  // outputValue = map(sensorValue, 0, 1023, 0, 255);
  // // change the analog out value:
  // analogWrite(ledOutPin, outputValue);
  // analogWrite(motorOutPin, outputValue);

  // print the results to the Serial Monitor:
  // Serial.print("sensor = ");
  // Serial.print(sensorValue);
  // Serial.print("\t output = ");
  // Serial.println(outputValue);

  // read the input pin:
  // int buttonState = digitalRead(pushButton);
  // analogWrite(ledOutPin, 255*buttonState);
  // print out the state of the button:
  // Serial.println(buttonState);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(30);
}
