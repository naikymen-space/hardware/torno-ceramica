# Reusar motores eléctricos

Ver:

- How To Wire Most Motors For Shop Tools and DIY Projects: 031 en <https://www.youtube.com/watch?v=ZKodxGcRSnw>

## Motor de inducción de dos fases (ventilador Liliana)

El ventilador que me dieron para reparar no tiene carbones.

Hay dos opciones para "brushless": imanes permanentes y motores de inducción.

Sin embargo, el rotor no parece tener imanes, y se parece más al de un motor de inducción de 3 fases (que saqué de un lavarropas).

Lo interesante es un capacitor conectado a algún lado del motor: ¿que papel cumple?

En circuitos AC, un capacitor introduce un "lag" de voltaje.

Buscando "fan two phase capacitor induction motor" en google me encuentro con un circuito para un motor de inducción de una fase (o sea, de lo que hay en los enchufes típicos de la pared).

- <https://www.researchgate.net/figure/Capacitor-Run-Single-Phase-Induction-Motor_fig3_241590054>
- <https://www.electrical4u.com/types-of-single-phase-induction-motor/>

![20220504-115605.png](images/20220504-115605.png)

En el segundo link explican algo:

Hay varios tipos de motores de 1 fase con "capacitores":

- Capacitor Start IM
- Capacitor Start Capacitor Run IM
- y otros...

La clave en general es esta:

> We already know that a single-phase induction motor is not self-starting because the magnetic field produced is not a rotating type. To produce a rotating magnetic field, there must be some phase difference.

## Motor Universal (de lavarropas)

Es un motor "universal": <https://en.wikipedia.org/wiki/Universal_motor>

Cableado:

- <https://www.instructables.com/id/How-to-Use-a-Washing-Machine-Motor/>
- <https://www.instructables.com/id/Washing-Machine-Motor-Wiring-Diagram/>
- <https://www.instructables.com/id/Make-your-own-Lathe-from-other-peoples-rubbish/>

El mio es así, pero tiene dos cables negros más, que quizás sean un termostato de protección.

Son bastante informativos:

- <https://www.youtube.com/watch?v=uFZsM5sOrj8>
- <https://youtu.be/xI6CF2nYf_g?t=66>
- <https://www.youtube.com/watch?v=tUQg2WiHpUY> Y <https://youtu.be/tUQg2WiHpUY?t=611>

- <https://youtu.be/tUQg2WiHpUY?t=460>
- <https://youtu.be/tUQg2WiHpUY?t=615>

- Este es como el mio: <https://youtu.be/CtulRqznbzI?t=388>

- <https://www.youtube.com/watch?v=SrPHQh-M3pM>

- Uno en hackaday: <https://hackaday.com/2017/12/20/reusing-motors-from-washing-machines/>

### Conexiones del motor

Del motor salen 9 cables, y se enganchan en un enchufe de 10 ranuras (una queda vacía).

Del enchufe salen cables con otros colores.

| Enchufe (motor) | Color cable (motor) | Parte       | Nota                 | Enchufe (de salida) |
|-----------------|---------------------|-------------|----------------------|---------------------|
| 10              | Rojo                | Field A ?   | R1 = 1 Ω; R7 = 2 Ω;  | Marrón              |
| 9               | Gris                | Brush A     | R8 = 4 Ω;            | Naranja             |
| 8               | Gris                | Brush B     | R9 = 4 Ω;            | Azul                |
| 7               | Negro               | Field B ?   | R1 = 1 Ω; R10 = 2 Ω; | Blanco              |
| 6               | Negro               | ?           | Conectado con #5     | -                   |
| 5               | Marrón              | ?           | Conectado con #6     | -                   |
| 4               | Blanco              | Tacometro A | -                    | Azul                |
| 3               | Blanco              | Tacometro B | -                    | Naranja             |
| 2               | -                   | -           | -                    |                     |
| 1               | Azul                | Field C ?   | R7 = 1 Ω; R10 = 1 Ω; | Rojo                |

Adivinanzas en base al [video](https://www.youtube.com/watch?v=CtulRqznbzI&feature=youtu.be&t=388):

- Los cables de los enchufes 6 y 5 están conectados, quizás sea un termostato. No es necesario usar estos cables para que ande.
- Las resistencias entre los cables 1, 10 y 7 (o 5) son compatibles con los field coils. Es posible que el cable 1 sea un punto medio entre las dos bobinas (o algo así), ya que tiene la mitad de la resistencia.

![conexiones_cables_motor.png](images/conexiones_cables_motor.png)

### Shunt VS Series winding: Torque y Velocidad

Hay una diferencia importante entre conectar los electroimanes en serie y en paralelo. Ver:

- <https://www.electricaleasy.com/2014/07/characteristics-of-dc-motors.html>
- <https://www.electrical4u.com/shunt-wound-dc-motor-dc-shunt-motor/>
- Muy buena demostracion: Inside a Washing Machine Motor: Explanation, Pinout, Teardown AND Experiments <https://youtu.be/CtulRqznbzI?t=501>

Cableado, torque y velocidad: Series vs Shunt wound motor

- <https://www.electricaleasy.com/2014/07/characteristics-of-dc-motors.html>
- <https://en.wikipedia.org/wiki/Saturation_(magnetic)>
- <https://electronics.stackexchange.com/a/343236>
- <https://electronics.stackexchange.com/a/554250>
- <https://electronics.stackexchange.com/a/372597>
- <https://electronics.stackexchange.com/a/141840>
- <https://www.physicsforums.com/threads/why-does-speed-of-dc-motor-increase-when-flux-is-reduced.804006/post-5047811>
- <https://www.physicsforums.com/threads/speed-control-by-flux-weakening.706529/post-4478852>

#### Prueba: alimentación independiente

En este caso, probé alimentar el rotor y el estator con dos fuentes diferentes. De esta manera podría ir viendo qué forma de conectarlo conviene para regular la velocidad frente a cambios de carga (o sea, mayor o menor torque).

Entre las 3 terminales del estator (field winding) use el par de mayor resistencia.

![draft.png](images/draft.png)

Probé alimentar el estator con 5V y 12V. Obtuve más torque con 12 V.

El rotor (armature winding) lo allimenté con voltaje variable, y andaba bien a 12V y 24V también.

En resumen: velocidad baja, torque alto.

### Tacómetro

Parece que manda 8 pulsos por vuelta; dedujimos eso usando el medidor de frecuencia del tester y la app de android Strobily como estroboscopio.

![20200611-222520.png](images/20200611-222520.png)

### Series winding: Circuito Diac-Triac discreto

Una forma de controlar el motor es con este circuito, en configuración "series" (el rotor en serie con el estator).

Recurso para circuitos analogicos: <http://educypedia.karadimov.info/library/CD00003856.pdf>

Buena explicación acá:

- Triac diac control: <https://www.electronics-tutorials.ws/power/triac.html>
- 0.1 μF capacitor explanation: <https://learnabout-electronics.org/Semiconductors/thyristors_64.php>
- Control del motor: <https://www.hackster.io/saulius-bandzevicius/arduino-based-universal-ac-motor-speed-controller-a4ceaf>
- Snubber circuit: <https://www.ppi-uk.com/news/snubber-mov-circuits-for-thyristor-assemblies/>

Armé uno básico con lo que tenía, para ver si podía prender un LED.

![20200603-010206.png](images/20200603-010206.png)

El triac **no es simétrico**, es importante que el voltaje de gate corresponda (de alguna manera) al de la terminal MT1 (o A1). Por lo que si se "da vuelta" el circuito (poniendo el motor en MT1, por ejemplo) no va a funcionar. Lo probé y lo que pasó fue que apenas le ponía algo de carga (el LED por ejemplo) el voltaje bajaba muchísimo.

![20200617-234059.png](images/20200617-234059.png)

![20200617-234158.png](images/20200617-234158.png)

#### Como controlar Triacs

- Achim Dobler: Controlling a TRIAC using an STM32, how hard can it be? <https://www.youtube.com/watch?v=uSq42wN4JIU>

#### Triac-diac con resistencia variable

Lo que cambié respecto de los diseños a continuación es que puse el "snubber" en paralelo a las terminales MT1 y MT2 del triac, en vez de lo que dicen las fotos. Según internet el snubber en paralelo con el triac funciona bien, pero la verdad es que no sé por qué es mejor (o sería mejor) poner el snubber como se muestra en las figuras a continuación.

[![20200529-073531.png](images/20200529-073531.png)](https://www.instructables.com/id/Make-your-own-Lathe-from-other-peoples-rubbish/)

[![20200529-074513.png](images/20200529-074513.png)](https://youtu.be/xI6CF2nYf_g?t=216])

[![20200602-170927.png](images/20200602-170927.png)](https://youtu.be/xI6CF2nYf_g?t=217)

### Series winding: Circuito de control digital I

El de abajo depende de un opto-triac y de un zero-cross detection circuit.

Este se parece más al analógico de arriba, que en vez de usar un potenciometro, usa un LDR. La resistencia de ese LDR es controlada por un LED, que es controlado por un Arduino. Una buena solución.

Video: <https://www.youtube.com/watch?v=7yEABsNyRfo>

### Series winding: Circuito de control digital II

#### Triac-diac con optocoplador para Arduino

En realidad, este circuito esta hecho para un light dimmer, pero quizás sirva.

<http://www.bristolwatch.com/arduino/arduino1.htm>

Es un poco raro que use un diac y el optocoupler 3011, porque este ultimo ya tiene un triac adentro. Y de hecho es así, ese diac puede omitirse: <https://youtu.be/oPiG3Qj_wWo?t=564>

En ese caso, el circuito es equivalente al siguiente...

#### Triac con optoacoplador para Arduino

Este es un diseño que serviría para controlar el motor con un Arduino.

![20200602-172227.png](images/20200602-172227.png)

De https://www.hackster.io/saulius-bandzevicius/arduino-based-universal-ac-motor-speed-controller-a4ceaf

Necesitaba un optocoplador MOC 3020/3021 y compré el equivocado (un 3041). La diferencia entre ellos es que el 3041 tiene "zero-crossing detection", o sea que solamente deja pasar corriente si el optocoplador esta activo durante la inversión del voltaje AC.

Info:

- [moc3021_datasheet.pdf](images/moc3021_datasheet.pdf)
- <https://www.elprocus.com/opto-couplers-types-applications/>
- <https://youtu.be/OIkQ6evY-mg?t=170>

**Nota**: en otro video del canal "post-apocalyptic inventor" el tipo muestra que su circuito con el tristor no es óptimo, es solo "half-wave" y no logró armar uno "full-wave" que anduviera bien. Así que me quedaría con el circuito que muestra FloweringElbow en su video.

**Nota**: el capacitor C4 y la resistencia R14 son un "snubber circuit", pero no es el único. Aparentemente hay otro snubber que viene de [este instructables](https://www.instructables.com/Arduino-controlled-light-dimmer-The-circuit/) formado por el capacitor C3. Según ese instructables, son dos "tipos" de circuito snubber.

##### Control con Arduino

Además del circuito que controla el motor, es importante armar dos circuitos más, para:

- Detectar el "zero crossing" de la corriente alterna de la pared.
- Medir la velocidad de rotación del motor usando el tacómetro con el que viene.

![20201228-170415.png](images/20201228-170415.png)

Para detectar el "zero-crossing", usa un optocoplador de manera similar a [este proyecto](https://circuitspedia.com/ac-dimmer-using-arduino/) de light dimmer.

Para medir los pulsos del tacómetro, usa un circuito comparador.

![20200611-115246.png](images/20200611-115246.png)

Def https://www.hackster.io/saulius-bandzevicius/arduino-based-universal-ac-motor-speed-controller-a4ceaf

Componentes clave:

- Optocoupler 3021/3023 (opto-triac) para disparar el triac del motor.
- Optocoupler PC817 (opto-transistor) para detectar el zero-crossing del voltaje AC.
- Comparador LM393 (dual differential comparator) para leer el tacómetro del motor.
- Una resistencia de 100 Ohm y 5W.
- Capacitores de 47 nF, 100 nF (x2), 220 nF (x1)

Además Muchas resistencias, capacitores y diodos.&lt;/todo&gt;

BOM completa: [universal_ac_motor_pid_control_clgotsovlr.zip](images/universal_ac_motor_pid_control_clgotsovlr.zip)

##### Mejoras al circuito

![20200529-073916.png](images/20200529-073916.png)

De: https://www.instructables.com/id/Make-your-own-Lathe-from-other-peoples-rubbish/

Links sacados del video y del instructable de FloweringElbow:

- PID library <https://playground.arduino.cc/Code/PIDLibrary/>
- Control con arduino: <https://www.hackster.io/saulius-bandzevicius/arduino-based-universal-ac-motor-speed-controller-a4ceaf>
- Lista de partes para control con arduino: <https://www.instructables.com/id/Multi-Purpose-Rotary-Machine-Mulling-Welding-Potte/#step21>
- Mi "application note" de los Triacs tiene mas info recopilada.

##### Half-wave control del "PAI"

![20200529-075838.png](images/20200529-075838.png)

De: https://youtu.be/tUQg2WiHpUY?t=971

## Motor Universal (de licuadora)

Probablemente sea de `600 W`, conectado a `220 V AC` con un potenciómetro de por medio. También tenía un interruptor que seguramente cerraba el circuito solamente cuando se colocaba una herramienta sobre la base de la licuadora (para [nunca girar libre](https://en.wikipedia.org/wiki/Universal_motor#Torque-speed_characteristics)).

El diámetro del eje es `6 mm`. Viene con una rosca, arandela y tuerca, que servían para ajustar la base de la licuadora.

Originalmente el engranaje se conectaba a una caja reductora "planetaria". Creo entonces que tenia dos tipos de herramientas: una con más RPM (conectada directamente al eje) y otra con menos, conectada a la caja reductora.

No estoy seguro pero creo que la potencia se regulaba con un selector de resistencias de algunos MegaOhm (Entre `20 MΩ` y `0.5 MΩ` aprox).

Como se ve en el video (hacia el final) el motor está bastante "trabado" y no gira debajo de 12 V. Ofrece bastante resistencia a la rotación. Le puse WD-40 y pareció ayudar, pero muy poco. Quizás sea así por diseño, quizás esté sucio.

https://www.youtube.com/watch?v=MCdyuEkksUE

### Circuitos de control

En principio se usa un pedal. Lo más probable es que en las máquinas "pro" eso controle la velocidad (y no la potencia) con PID o algo asi.

Intenté usar un motor NEMA 17 y un Arduino, pero el torque de ese motor es muy pequeño. En cambio, el motor de licuadora debería tener suficiente punch!

Para controlarlo con PID debería armar un circuito similar al del motor de lavarropas, que es del mismo tipo. Por ahora (01/2021) no logré hacerlo andar :/ asi que me quedo con el analógico que solo tiene control de potencia.

> Nota: podría usar un stepper o algo asi para hacer control closed-loop moviendo el potenciometro, pero lo dejo como alternativa.

El circuito analógico es el mismo que hice para el motor de lavarropas:

- <https://wiki.frubox.org/proyectos/diy/upcycle/electric_motors#triac-diac-con-resistencia-variable>
- <https://crcit.net/c/e5a0dfb26c6246c1a83dc35713a4accf>

Respecto al [circuito original](https://www.youtube.com/watch?t=217&v=xI6CF2nYf_g&feature=youtu.be), hay varias desviaciones:

- No está el [varistor](https://es.wikipedia.org/wiki/Varistor).
- La resistencia entre el Diac y el potenciometro es 225 kOhm (en vez de 22 kOhm).
- La resistencia entre el motor y el potenciómetro no está.
- C1 es de 47 nF (en vez de 100 nF) y C2 es de 100 nF (en vez de 33 nF).
- No tiene el snubber!

![20210106-114316.png](images/20210106-114316.png)

![20210106-120641.png](images/20210106-120641.png)

Correccion: el diac es "DB3" (no DB2).

En mis pruebas, funciona, pero no es ideal. A veces salta.

### Circuito #4

En el [application note](http://educypedia.karadimov.info/library/CD00003856.pdf) de STM dice que lo mejor es hacer el "circuito #4" que menciona FloweringElbow [en su video](https://youtu.be/xI6CF2nYf_g?t=407):

![20210106-122814.png](images/20210106-122814.png)

_TRIAC analog control circuits for inductive loads._

## Motor de lavarropas de induccion de tres fases

Es interesante! el rotor no tiene conexiones ni imanes permanentes.

Para moverlo hay que generar corriente alterna de 3 fases, y necesitaria armar un "3 phase inverter". Un "inverter" invierte corriente contínua en conrriente alterna de una fase. Hay inverters comerciales como el "VFD" que se conectan a 220 VAC de una fase (la pared) y sacan 220 VAC de tres fases.

Buena explicación acá:

- VFDs <https://youtu.be/yEPe7RDtkgo?t=441>
- Usan transistores IGBT: <https://www.youtube.com/watch?v=RxRJW09A_XA>

Ejemplos de circuito inverter:

- <https://www.instructables.com/How-to-Make-an-Inverter-Using-ARDUINO/>
- <https://create.arduino.cc/projecthub/user534361260/make-your-own-power-inverter-using-arduino-666cf9>

Hay inverters cuadrados (como el anterior) y "pure sinewave". Se supone los de onda sinoidal son menos ruidosos, la onda es igual a la que sale de la pared. Ver este circuito:

- <https://www.homemade-circuits.com/arduino-pure-sine-wave-inverter-circuit/>
- <https://circuitdigest.com/microcontroller-projects/pure-sine-wave-generator-using-arduino>

Más info:

- <https://www.youtube.com/watch?v=TumJjfcVVuo>
- <https://www.youtube.com/watch?v=ASPwNEtSzK8>
- <https://www.youtube.com/watch?v=TAFDX301Qrk>
- Este compra el inverter, no lo arma: <https://www.youtube.com/watch?v=MTGhQMxSTQw>
- <https://www.electrical4u.com/working-principle-of-three-phase-induction-motor/>
- <https://electronics-project-hub.com/three-phase-inverter-circuit-diagram/>
- <https://www.google.com/search?tbm=isch&q=3+phase+arduino+inverter>

### Varios Circuitos para inverters de tres fases

- 3 phase inverter controlled by an Arduino ATmega328 <https://www.youtube.com/watch?v=fHoEmjUX8_8>
- STM32 5kW 3-Phase Motor Controller <https://www.youtube.com/watch?v=0NocvqFzyv8>
    - <http://embeddedlightning.com/frequenzumrichter-3-phasenbrucke/>
- Three Ways To Run A Three Phase Motor On Single Phase, And the Pro's and Con's of Each Method #065 <https://www.youtube.com/watch?v=cE34YHsRY88>
- Simple VFD 3 Phase Induction Motor SPWM using Arduino UNO <https://www.youtube.com/watch?v=HNj1gP-Tqf8>
    - Preliminary experiment with SPWM 3 Phase Induction Motor (SPWM con un filtro RC de paso bajo) <https://www.youtube.com/watch?v=kP9gJZ7__64>
- Y los que están abajo...

### Circuito 1 - Ilusys Systems

Muy buena explicación.

Designing 3-phase motor driver: <https://www.youtube.com/watch?v=TAFDX301Qrk>

BOM:

- Power MOSFET ([IRLB8721PbF](https://cdn-shop.adafruit.com/datasheets/irlb8721pbf.pdf))
    - IRF3205 parece bueno <https://components101.com/mosfets/irf3205-pinout-datasheet>
    - Recomiendan IRF540n para Arduino <https://components101.com/irf540n-pinout-equivalent-datasheet>
- HIGH AND LOW SIDE DRIVER ([IRS2101(S)PbF](https://www.mouser.com/datasheet/2/196/Infineon-IRS2101-DataSheet-v01_00-EN-1732829.pdf))
    - En ML esta el [IR2021](https://www.infineon.com/dgdl/Infineon-ir2101-DS-v01_00-EN.pdf?fileId=5546d462533600a4015355c7a755166c) que se parece bastante.
    - Quizas el [IR2110](https://articulo.mercadolibre.com.ar/MLA-705259024-driver-mosfet-igbt-500v-dip14-ir-2110-ir-2110-ir2110-_JM) sea mejor, pero las conexiones son diferentes.
    - Mcp1407 alta velocidad, parece bueno.
    - Tc4421 alta velocidad, parece bueno.
- Optoacopladores ([6N137S-L](https://optoelectronics.liteon.com/upload/download/DS70-2008-0035/6N137-L%20series%20Oct%202017%20RevD.PDF))
- Varios capacitores, resistores y diodos.
- Hall sensors.

### Circuito 2 - Blogthor

<https://electronics-project-hub.com/three-phase-inverter-circuit-diagram/>

- IRF9540
- IRF540
- BC 577
- BC 548
- Transformadores 0-9V/220V/5A

### Circuito 3 - Pantech

<https://www.youtube.com/watch?v=M8Ae5n2A3RY>

### Circuito 4 - Kash Patel

- Driver <https://www.youtube.com/watch?v=2rwKnnJtt0c>
- Arduino UNO: <https://www.youtube.com/watch?v=8QxS0dKqnSc>
- Arduino Mega: <https://www.youtube.com/watch?v=aYWMUo1fbOE>

Hay dos codigos, el más simple parece ser el del arduino UNO, que repite el sketch de LED brightness tres veces, y los hace loopear con contadores "fuera de fase" (uno arranca en -170, otro en 0, y el último en 170). Imagino que eso tiene algo que ver con el desfasaje de 120º.

El del arduino mega tiene más cosas: botones, perilla, display, etc. Además parece que tiene un "dead time" agregado, para evitar que los dos IGBTs de una fase se activen a la vez por error.

### Reusar un ESC

El ESC es el driver de los motores BLDC, típicos de avioncitos y drones.

Esos tienen un feature en el firmware, que hay que deshabilitar, para que anden con un motor de tres fases: <https://electronics.stackexchange.com/a/251099>

Hay un repo con firmwares modificables para varios drivers: <https://github.com/sim-/tgy>

Tendría que ver bien en qué consiste esa modificiación.

# Aplicaciones

## Torno para cerámica

Video ilustrativo: <https://www.instagram.com/p/CIBUBnYjyUs>

Video explicativo: <https://www.youtube.com/watch?v=LqlGoTwVD4g>

Más videos:

- <https://www.youtube.com/watch?v=ZMy0MIDemzk>
- <https://www.youtube.com/watch?v=akoy2MCvIXo>

Características importantes:

- Hasta `1 Kg` de arcilla.
- Velocidad máxima: `240 RPM`
- Control de velocidad (con pedal).
- Mecanismos y electrónica protegida del agua y de la arcilla.
- Diámetro aproximado: `30 cm`
- El plato y la velocidad deben ser estables (no temblar, ni frenarse).

### Diseño

Normalmente se diseñan como exprimidores de naranja (con un balde que tiene un agujero en el medio para el eje del motor, que está abajo).

Pero ese diseño neceita partes demasiado específicas, es más fácil conseguir una palangana, así que pensé en esto:

  ![20201202-060355.png](images/20201202-060355.png)

En gris el motor, en rojo la polea, en negro la mesa que gira. En azul rulemanes, para que el plato gire sobre la base como si fuera el plato del microondas (que esta en amarillito).

El problema que tiene es que hay que proteger el motor del agua. Pensé que con una bolsa de aspiradora de esas que no dejan pasar el agua (pero si el aire) iría bien.

Probé con un NEMA 17 pero el torque es demasiado pequeño, se traba apenas con algo de resistencia :(

Intenté acoplar el motor de licuadora con poleas GT2 y poleas impresas en 3D. "Funcionó", pero vibraba un montón.

Dada mi experiencia con el shunt winding de un motor universal, creo que va a ser mejor tener un motor grandote al costado y listo.

### Motor de inducción VS universal

El universal en "series winding" sería más simple de controlar en cuanto a electrónica (aunque no fácil), pero el de inducción no puede pasarse de revoluciones.

Por eso creo que es más seguro usar uno de inducción, y aprender a armar un cricuito trifásico (dado que no pude hacer andar el circuito PID para el motor universal, además) quizás sea divertido.

Por otro lado el "shunt winding" de un motor universal anda bastante bien, a bajo voltaje, y parece fácil de regular. Así que no sería una mala idea, y probablemente me quede con esto.

Algunas cuentitas de plata:

Una torneta nueva está 5.000 pesos y un torno chico está 120.000 pesos (2021-2022). Un motor de inducción (usado) sale entre 5 y 10 lucas, así que cualquier adaptación funcional de uno de esos deja un margen de al menos 100.000 pesos.

Algunos motores de inducción (también llamados "trifásicos") usados en venta:

- <https://articulo.mercadolibre.com.ar/MLA-660237524-recambio-motor-lavarropas-todas-las-marcas-_JM>
- <https://articulo.mercadolibre.com.ar/MLA-908650474-motor-de-induccion-_JM>
- <https://articulo.mercadolibre.com.ar/MLA-920244289-induccion-motor-100-v-marca-oriental-motors-_JM>

Poleas y correas para lavarropas:

- Poleas: <https://listado.mercadolibre.com.ar/lavarropas-secarropas-repuestos-accesorios-poleas/>
- Correas: <https://listado.mercadolibre.com.ar/lavarropas-secarropas-repuestos-correas/>
- Un conjunto: <https://articulo.mercadolibre.com.ar/MLA-1108424968-reemplazo-correa-1212-drean-blue-correa-1173-polea-27cm-_JM>

### Circuito de control DC

Ver:

- <https://electronics.stackexchange.com/a/356698>
- "Simple Power MOSFET Motor Controller" <https://www.electronics-tutorials.ws/transistor/tran_7.html>
    - "//a simple flywheel diode is connected across the inductive load to dissipate any back emf generated by the motor when the MOSFET turns it “OFF”. *". \* "*For added security an additional silicon or zener diode D1 can also be placed across the channel of a MOSFET switch ... for suppressing over voltage switching transients and noise giving extra protection to the MOSFET switch if required.//"
    - Sugerencias de Vgs y como cablear: <https://electronics.stackexchange.com/questions/571302/pwm-motor-control-mosfet-getting-very-hot>
    - Cuentitas para Vgs: <https://electronics.stackexchange.com/questions/322522/high-voltage-pwm-motor-controller-mosfets-explode>
    - Consideraciones de PWM: <https://electronics.stackexchange.com/questions/252189/pwm-strategy-for-motor-with-variable-load?rq=1>
- <https://electronics.stackexchange.com/questions/268305/how-can-i-reduce-the-pwm-noise-of-a-blower-motor>
    - Segun ese ultimo, puedo manejar el MOSFET [IRF3205](https://components101.com/mosfets/irf3205-pinout-datasheet) con un transistor y el arduino detrás.
    - Usa un transistor [BC457](https://components101.com/transistors/bc547-transistor-pinout-datasheet), que parece que puedo reemplazar por un [2N2222](https://components101.com/transistors/2n2222a-pinout-equivalent-datasheet) que si tengo.
    - Dice que el diode [1N4007](https://www.vishay.com/docs/88503/1n4001.pdf) que tengo no se la banca como "[clamp diode](https://en.wikipedia.org/wiki/Flyback_diode)" de un motor, porque la corriente máxima es muy baja (1A) y recomiendan usar un diodo "schottky".
- Capacitores:
    - <https://electronics.stackexchange.com/a/562515>
    - <https://electronics.stackexchange.com/questions/127500/eliminate-pwm-noise-in-pwm-driven-bidirectional-motor>

### Sistema de transmisión

Venden correas acá: <https://g.page/Servibaires>

Compré una cualquiera pero no tuve suerte (oh sorpresa), porque los dientitos no coinciden.

Asi que fui con el motor a [este local](https://g.page/Servibaires?share) y me dieron una que funcionó mejor:

![20220530-043907.png](images/20220530-043907.png)

Para los rodamientos usé un par de estos:

- "Soporte Autocentrante Con Ruleman Para Eje De 12mm Ucf 201"
- <https://articulo.mercadolibre.com.ar/MLA-836625358-soporte-autocentrante-con-ruleman-para-eje-de-12mm-ucf-201-_JM>
